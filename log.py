import datetime

log_file = "log.log"

def log(text):
    limit = 2000
    if not str(datetime.date.today().year) + "-" in text:
        text = str(datetime.datetime.now()) + ": " + text
    if not "\n" in text:
        text += "\n"
    with open(log_file) as logf:
        lines = logf.readlines()
        lines.append(text)
    
    with open(log_file, "w") as logf:
        logf.write("".join(lines[max(0, len(lines) - limit):len(lines)]))

def printlog(text):
    print(text)
    log(text)