from bs4 import BeautifulSoup
import re
import datetime

def parse(html):
    months = ["", "January", "February", "March", "April",
              "May", "June", "July", "August", "September",
              "October", "November", "December"]
    months_re = "(" + "|".join(months[1:]) + ")"
    full_re = months_re + " ([0-3]?[0-9])"
    year_re = "(20[0-9][0-9])"

    soup = BeautifulSoup(html, "html.parser")
    tables = soup.find_all("table")
    events = []
    for table in tables:
        cells = table.find_all("td")
        for i in range(len(cells)):
            if cells[i].text.lower() in ["sale start", "event start"]:
                type_ = "sale" if "sale" in cells[i].text.lower() else "event"
                start_search = re.search(full_re, cells[i+1].text)
                start_year_search = re.search(year_re, cells[i+1].text)
            if cells[i].text.lower() in ["sale end", "event end"]:
                end_search = re.search(full_re, cells[i+1].text)
                end_year_search = re.search(year_re, cells[i+1].text)
        if start_search and end_search:
            if start_year_search:
                start_year = int(start_year_search.group(0))
            else:
                start_year = datetime.datetime.today().year
            if end_year_search:
                end_year = int(end_year_search.group(0))
            else:
                end_year = datetime.datetime.today().year
            start_time = datetime.datetime(
                start_year,
                months.index(start_search.group(1)),
                int(start_search.group(2)),
                10)
            end_time = datetime.datetime(
                end_year,
                months.index(end_search.group(1)),
                int(end_search.group(2)),
                10)
            events.append(
                {"start": start_time,
                 "end": end_time,
                 "type": type_})
    if any(events):
        for he in ["h1"]:
            headers = soup.find_all(he)
            if (len(headers) == len(events)
                or len(headers) == len(events) + 1):
                for x in range(len(events)):
                    events[x]["name"] = headers[x].text
                break
        return events
            

    l = locals()
    for x in l:
        if not x in globals():
            globals()[x] = l[x]

if __name__ == "__main__":
    import os
    for x in os.listdir():
        if ".html" in x:
            with open(x) as f:
                print(x)
                print(parse(f.read()))
