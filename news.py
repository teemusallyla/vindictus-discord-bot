import aiohttp
import asyncio
import json
import discord
from discord.ext import tasks, commands
from bs4 import BeautifulSoup
from new_parser import parse
from event import Event
import async_timeout

from log import log
from log import printlog

news_link = "http://vindictus.nexon.net/news/all/"
news_log_length = 35

with open("news.json") as news_json:
    news = json.load(news_json)
    
class NewsCog(commands.Cog):

    def __init__(self, bot, max_backlog=1):
        self.bot = bot
        self.get_news.start()
        self.max_backlog = max_backlog


    @tasks.loop(minutes=1)
    async def get_news(self):
        new_news = {"news": []}
        news_raw = []

        for i in range(self.max_backlog):
            try:
                with async_timeout.timeout(10):
                    async with aiohttp.ClientSession() as session:
                        async with session.get(news_link + str(i + 1)) as response:
                            resText = await response.text()
            except asyncio.TimeoutError:
                return
            
            soup = BeautifulSoup(resText, "html.parser")
            news_raw = news_raw + soup.find_all("div", class_="news-list-item")

        self.max_backlog = 1

        for news_piece in news_raw:
            news_item = {}
            news_item["title"] = news_piece.find(class_ = "news-list-item-title").text\
                .replace("\r", "").replace("\n", "").replace("\t", "").replace("  ", "")
            news_item["description"] = news_piece.find(class_ = "news-list-item-text").text\
                .replace("\r", "").replace("\n", "").replace("\t", "").replace("  ", "")
            news_item["link"] = "http://vindictus.nexon.net" + news_piece.find(class_ = "news-list-link").get("href")
            news_item["image"] = news_piece.find(class_ = "news-thumbnail").attrs["style"][22:-2]
            new_news["news"].append(news_item)
        
        news_list = new_news["news"]
        news_list.reverse()
        for news_piece in news_list:
            if not news_piece in news["news"]:
                await self.post_news(news_piece)
                news["news"].append(news_piece)
                log("New news found")
        if new_news["news"] != []:
            with open("news.json", "w") as news_json:
                news["news"] = news["news"][max(0, len(news["news"]) - news_log_length):]
                json.dump(news, news_json, indent=4)
        log("News gotten")

    @get_news.before_loop
    async def wait_for_ready(self):
        await self.bot.wait_until_ready()

    async def post_news(self, item):
        title = item["title"]
        link = item["link"]
        desc = item["description"]
        news_id = link.split("/")[4]
        emb = discord.Embed(
            title = title + " - Vindictus",
            description = desc,
            color = 133916,
            url = link
        ).set_thumbnail(
            url=item["image"]
        ).set_author(
            name="Vindictus - Official Website",
            url="https://vindictus.nexon.net"
        )

        for message in self.bot.sent_messages:
            if message["id"] == news_id:
                id_dict = message
                break
        else:
            id_dict = {"id": news_id}
            self.bot.sent_messages.append(id_dict)

        maint = "maintenance" in title.lower() or "maintenance" in desc.lower()
        win_update = "windows update" in title.lower() or "windows update" in desc.lower()
        completed_maint = (maint or win_update) and "complete" in desc.lower()
        extended_maint = (maint or win_update) and "extend" in desc.lower()
        await self.bot.wait_until_ready()
        for channel in self.bot.post_channels:
            if not str(channel.id) in id_dict or completed_maint or extended_maint:
                sent_message = await channel.send(embed=emb)
                printlog("Sent: " + title)
                id_dict[str(channel.id)] = sent_message.id
            else:
                try:
                    previous_message = await channel.fetch_message(int(id_dict[str(channel.id)]))
                    await previous_message.edit(embed=emb)
                    printlog("Edited: " + title)
                except discord.NotFound:
                    printlog("Tried to look for a message, not found")
                except discord.Forbidden:
                    printlog("Tried to look for a message, not allowed")
                except discord.HTTPException:
                    printlog("Tried to look for / edit a message, couldn't")
        self.bot.sent_messages = self.bot.sent_messages[max(0, len(self.bot.sent_messages) - news_log_length):]
        with open("messages.json", "w") as messages_json:
            json.dump({"messages": self.bot.sent_messages}, messages_json, indent=4)

        await self.parse_events(link)

    async def parse_events(self, link):
        async with aiohttp.ClientSession() as session:
            async with session.get(link) as response:
                resp = await response.text()
        
        events, sales = parse(resp)
        events = [
            Event(e["name"], e["start_date"], e["end_date"], link) for e in events
        ]
        sales = [
            Event(e["name"], e["start_date"], e["end_date"], link) for e in sales
        ]
        self.bot.events.extend(events)
        self.bot.sales.extend(sales)

        filtered_list = []
        names_list = []
        for event in reversed(self.bot.events):
            if not event.has_finished() and not event.name in names_list:
                filtered_list.append(event)
                names_list.append(event.name)
        self.bot.events = list(reversed(filtered_list))
        filtered_list = []
        names_list = []
        for sale in reversed(self.bot.sales):
            if not sale.has_finished() and not sale.name in names_list:
                filtered_list.append(sale)
                names_list.append(sale.name)
        self.bot.sales = list(reversed(filtered_list))

        #self.bot.events = [e for e in self.bot.events if not e.has_finished()]
        #self.bot.sales = [e for e in self.bot.sales if not e.has_finished()]

        with open("events.json", "w+") as f:
            json.dump({
                "events": [e.to_json() for e in self.bot.events],
                "sales": [e.to_json() for e in self.bot.sales]
            }, f, indent=4)

    @commands.command()
    async def clear(self, ctx):
        if ctx.author != self.bot.owner:
            return
        self.bot.events = []
        self.bot.sales = []
        with open("events.json", "w+") as f:
            json.dump({
                "events": [],
                "sales": []
            }, f, indent=4)
        await ctx.send("Events cleared")

    @commands.command()
    async def refresh(self, ctx):
        if ctx.author != self.bot.owner:
            return
        await ctx.send("Refreshing")
        urls = []
        for news_piece in news["news"]:
            if not news_piece["link"] in urls:
                urls.append(news_piece["link"])
        for url in urls:
            await self.parse_events(url)
        await ctx.send("Events refreshed")

