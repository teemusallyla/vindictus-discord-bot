import discord
import json
from discord.ext import commands

class PrivateCog(commands.Cog):
    def __init__(self):
        self.role_names = ["balor", "neam"]
        self.private_guilds = []
        with open("private_guilds.txt") as f:
            for line in f.readlines():
                self.private_guilds.append(int(line.strip()))
    
    @commands.command(aliases=["balor", "neam"])
    async def addrole(self, ctx):
        if not ctx.guild.id in self.private_guilds:
            return
        if not ctx.me.guild_permissions.manage_roles:
            await ctx.send("I don't have the permissions")
            return
        if ctx.invoked_with.lower() not in self.role_names:
            return

        role_name = ctx.invoked_with.lower()
        role = discord.utils.find(
            lambda r: r.name.lower() == role_name, ctx.guild.roles
        )
        if role:
            if role in ctx.author.roles:
                try:
                    await ctx.author.remove_roles(role)
                    await ctx.message.add_reaction("✅")
                except discord.Forbidden:
                    await ctx.send("The requested role is above mine, cannot manage it")
            else:
                try:
                    await ctx.author.add_roles(role)
                    await ctx.message.add_reaction("✅")
                except discord.Forbidden:
                    await ctx.send("The requested role is above mine, cannot manage it")

    @commands.command()
    async def update(self, ctx):
        if not ctx.guild.id in self.private_guilds:
            return
        ec = ctx.bot.get_cog("EventsCog")
        event_emb = await ec.post_events(ctx, "events", True)
        sale_emb = await ec.post_events(ctx, "sales", True)
        try:
            with open("ongoing_messages.json") as f:
                ongoing_json = json.load(f)
                channel = ctx.bot.get_channel(
                    ongoing_json["channel_id"]
                )
                if not channel:
                    raise FileNotFoundError
                events_msg = await channel.fetch_message(
                    ongoing_json["events_id"]
                )
                sales_msg = await channel.fetch_message(
                    ongoing_json["sales_id"]
                )
                await events_msg.edit(embed=event_emb)
                await sales_msg.edit(embed=sale_emb)
        except (FileNotFoundError, discord.NotFound):
            channel = None
            events_msg = None
            sales_msg = None
            def check(m):
                return m.author == ctx.author
            await ctx.send("File/messages not found")
            while not channel:
                await ctx.send("Enter the channel id for the messages:")
                msg = await ctx.bot.wait_for("message", timeout=30, check=check)
                channel_id = int(msg.content)
                channel = ctx.bot.get_channel(channel_id)
            while channel and not events_msg:
                await ctx.send("Enter the id for the events message:")
                msg = await ctx.bot.wait_for("message", timeout=30, check=check)
                try:
                    events_id = int(msg.content)
                    events_msg = await channel.fetch_message(events_id)
                except:
                    pass
            while channel and events_msg and not sales_msg:
                await ctx.send("Enter the id for the sales message:")
                msg = await ctx.bot.wait_for("message", timeout=30, check=check)
                try:
                    sales_id = int(msg.content)
                    sales_msg = await channel.fetch_message(sales_id)
                except:
                    pass
            if channel and events_msg and sales_msg:
                with open("ongoing_messages.json", "w+") as f:
                    json.dump(
                        {"channel_id": channel_id,
                        "events_id": events_id,
                        "sales_id": sales_id},
                        f,
                        indent=4
                    )
                
                await ctx.reinvoke()

        except (discord.Forbidden, discord.HTTPException):
            await ctx.send("Fetching not allowed or failed")

        