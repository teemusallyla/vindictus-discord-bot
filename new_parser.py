import requests
import re
import datetime
from bs4 import BeautifulSoup

def parse(html):
    months = ["", "January", "February", "March", "April",
              "May", "June", "July", "August", "September",
              "October", "November", "December"]
    months_re = "(" + "|".join(months[1:]) + ")"
    full_re = months_re + " ([0-3]?[0-9])"
    year_re = "(20[0-9][0-9])"
    days_re = "([0-3]?[0-9])"

    slash_month_re = "[0-1]?[0-9](?=/)"
    slash_day_re = "(?<=/)[0-3]?[0-9]"
    
    soup = BeautifulSoup(html, "html.parser")

    strongs = soup.find_all("strong")
    spans = soup.find_all("span")
    is_event = lambda s: any(re.findall("(sale|event)s? period", s, re.IGNORECASE))
    event_strongs = [s for s in strongs if s.string and is_event(s.string)]
    event_spans = [s for s in spans if s.string and is_event(s.string)]
    events = []

    for event in event_strongs + event_spans:
        h1 = event.parent.findNextSibling("h1")
        if not h1:
            h1 = event.parent.parent.findPreviousSibling("h1")
        if not h1:
            h1 = event.parent.parent.parent.parent.findPreviousSibling("h1")
        if h1:
            name = h1.text
            if event in event_strongs:
                period_text = event.parent.text
            else: # There is no <strong> around it the "event period" text
                period_text = event.text
            
            # Try to find dates that are in the form of 12/30
            found_months = re.findall(slash_month_re, period_text)
            found_dates = re.findall(slash_day_re, period_text)

            if len(found_months) == 2 and len(found_dates) == 2:
                start_month = months[int(found_months[0])]
                end_month = months[int(found_months[1])]
            else:
                found_months = re.findall(months_re, period_text)
                found_dates = re.findall(days_re, period_text)
                if not found_months or not found_dates:
                    continue
                start_month = found_months[0]
                if len(found_months) in [2, 4]:
                    end_month = found_months[1]
                else:
                    end_month = found_months[0]

            if len(found_dates) < 2:
                print("Incorrect date: " + name)
                continue
            start_date = datetime.datetime(
                datetime.date.today().year,
                months.index(start_month),
                int(found_dates[0]),
                10
                )
            end_date = datetime.datetime(
                datetime.date.today().year,
                months.index(end_month),
                int(found_dates[1]),
                10
                )
            if start_date > end_date:
                # ending year is the coming year
                end_date = end_date.replace(year=end_date.year + 1)
            e = {
                "name": name,
                "start_date": start_date,
                "end_date": end_date,
                "type": "event" if "Event" in event.text else "sale"
                }
            events.append(e)

    sales = [e for e in events if e["type"] == "sale"]
    events = [e for e in events if e["type"] == "event"]
    return events, sales

if __name__ == "__main__":
    import sys
    if len(sys.argv) == 2:
        idd = sys.argv[1]
        url = "http://vindictus.nexon.net/news/" + idd
    else:
        url = "http://vindictus.nexon.net/news/52038/full-beach-package"
        url = "http://vindictus.nexon.net/news/52044/summer-daily-mission-event-more"
        url = "http://vindictus.nexon.net/news/52040/the-golden-summer-special-time-event"
        url = "http://vindictus.nexon.net/news/52037/summer-crystal"
        url = "http://vindictus.nexon.net/news/52035/guild-house-dungeon-improvements"
    resp = requests.get(url)
    events = parse(resp.text)
    print(events)

