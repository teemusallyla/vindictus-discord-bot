import datetime
import discord
import asyncio
import re
import json
from discord.ext import commands

months_re = "January|February|March|April|May|June|July|August|September|October|November|December"
months_array = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
days_re = "([0-9]|)[0-9]"
years_re = "20[0-9][0-9]"

class Event:
    def __init__(self, name=None, start=None, end=None, link=None, jjson=None):
        self.name = name
        self.start = start
        self.end = end
        self.url = link

        if jjson != None:
            self.from_json(jjson)

    def is_going_on(self):
        return self.start < datetime.datetime.now() < self.end

    def has_finished(self):
        return datetime.datetime.now() > self.end

    def is_new(self):
        return datetime.timedelta() < datetime.datetime.now() - self.start < datetime.timedelta(days=3)

    def print_self(self):
        print(self.name)
        print(self.start)
        print(self.end)

    def to_json(self):
        return {
            "name": self.name,
            "url": self.url,
            "start": self.start.timestamp(),
            "end": self.end.timestamp()
            }

    def from_json(self, jjson):
        self.name = jjson["name"]
        self.url = jjson["url"]
        self.start = datetime.datetime.fromtimestamp(jjson["start"])
        self.end = datetime.datetime.fromtimestamp(jjson["end"])


class EventsCog(commands.Cog):

    async def post_events(self, ctx, typ, get_emb=False):
        if typ == "events":
            going_on = [e for e in ctx.bot.events if e.is_going_on()]
        else:
            going_on = [s for s in ctx.bot.sales if s.is_going_on()]
        
        emb = discord.Embed(colour=discord.Colour(int("020b1c", 16)))
        emb.set_author(
            icon_url="https://cdn.discordapp.com/attachments/344883962612678657/399689459081150485/vindidiscord.png",
            name="Vindictus "+ typ.capitalize()
        )
        for event in going_on:
            start_month = months_array[event.start.month].capitalize()
            end_mont = months_array[event.end.month].capitalize()
            start_date = start_month + " " + str(event.start.day)
            end_date = end_mont + " " + str(event.end.day)
            name = event.name
            if event.is_new():
                name += " (New!)"
            emb.add_field(
                name=name,
                value="{} - {}. [Link]({})".format(start_date, end_date, event.url),
                inline=False
            )
        if get_emb:
            return emb
        else:
            await ctx.send(embed=emb)

    
    @commands.command()
    async def events(self, ctx):
        await self.post_events(ctx, "events")

    @commands.command()
    async def sales(self, ctx):
        await self.post_events(ctx, "sales")

    def parse_date(self, txt):
        mon = re.search(months_re, txt)
        day = re.search(days_re, txt)
        if mon != None and day != None:
            mon = months_array.index(mon.group())
            date = datetime.datetime(
                datetime.date.today().year,
                mon,
                int(day.group()),
                10
            )
            return date
        return None

    @commands.command()
    async def addevent(self, ctx):
        perm = ctx.author.guild_permissions
        owner = ctx.bot.owner
        if not perm.manage_messages and not owner:
            return

        to_del = [ctx.message]
        bot = ctx.bot
        timeo = 10
        sender = ctx.author
        channel = ctx.channel
        check = lambda m: m.author == sender and m.channel == channel
        event_type = None
        try:
            while event_type == None:
                m = await ctx.send("Enter type (event / sale)")
                to_del.append(m)
                resp = await bot.wait_for("message", check=check, timeout=timeo)
                to_del.append(resp)
                if resp.content.lower() in ["event", "sale"]:
                    event_type = resp.content.lower()
            
            event_name = None
            m = await ctx.send("Enter {} name".format(event_type))
            to_del.append(m)
            resp = await bot.wait_for("message", check=check, timeout=timeo)
            to_del.append(resp)
            event_name = resp.content
            
            start_date = None
            while start_date == None:
                m = await ctx.send("Enter starting date")
                to_del.append(m)
                resp = await bot.wait_for("message", check=check, timeout=timeo)
                to_del.append(resp)
                start_date = self.parse_date(resp.content)

            end_date = None
            while end_date == None:
                m = await ctx.send("Enter ending date")
                to_del.append(m)
                resp = await bot.wait_for("message", check=check, timeout=timeo)
                to_del.append(resp)
                end_date = self.parse_date(resp.content)
            
            link = None
            m = await ctx.send("Enter event link")
            to_del.append(m)
            resp = await bot.wait_for("message", check=check, timeout=timeo)
            to_del.append(resp)
            link = resp.content

            e = Event(event_name, start_date, end_date, link)
            bot.events.append(e) if event_type == "event" else bot.sales.append(e)
            sales_not_finished = [sale for sale in bot.sales if not sale.has_finished()]
            events_not_finished = [event for event in bot.events if not event.has_finished()]
            with open("events.json", "w+") as f:
                json.dump({
                    "events": [event.to_json() for event in events_not_finished],
                    "sales": [sale.to_json() for sale in sales_not_finished]
                }, f, indent=4)
            sender_name = sender.nick or sender.name
            await ctx.send("{} added a new {}: {}".format(
                sender_name, event_type, event_name
            ))
        except asyncio.TimeoutError:
            await ctx.send("Stopped adding a new event")
        finally:
            try:
                await ctx.channel.delete_messages(to_del)
            except:
                for msg in to_del:
                    try:
                        await msg.delete()
                    except:
                        pass

    @commands.command()
    async def editevent(self, ctx):
        perm = ctx.author.guild_permissions
        owner = ctx.bot.owner
        if not perm.manage_messages and not owner:
            return
        check = lambda m: m.author == ctx.author
        e = None
        timeout = 20
        try:
            while not e:
                await ctx.send("Enter the name of the event to edit:")
                msg = await ctx.bot.wait_for("message", timeout=timeout, check=check)
                for event in ctx.bot.events + ctx.bot.sales:
                    if event.name.lower() == msg.content.lower():
                        e = event
                        break
                else:
                    await ctx.send("Couldn't find an event with that name")
            
            if e:
                ft = None
                await ctx.send("Which feature should be edited? (name, start date, end date, url)")
                while not ft:
                    msg = await ctx.bot.wait_for("message", timeout=timeout, check=check)
                    cnt = msg.content.lower()
                    if "name" in cnt:
                        await ctx.send("Enter new name for " + e.name)
                        msg = await ctx.bot.wait_for("message", timeout=timeout, check=check)
                        e.name = msg.content
                        ft = "Name"
                    elif "end" in cnt:
                        end_date = None
                        while not end_date:
                            await ctx.send("Enter new ending date for " + e.name)
                            msg = await ctx.bot.wait_for("message", timeout=timeout, check=check)
                            end_date = self.parse_date(msg.content)
                            if not end_date:
                                await ctx.send("Couldn't parse date")
                                continue
                            if end_date < e.start:
                                end_date = end_date.replace(year=end_date.year + 1)
                            e.end = end_date
                            ft = "End date"
                    elif "start" in cnt:
                        start_date = None
                        while not start_date:
                            await ctx.send("Enter new starting date for " + e.name)
                            msg = await ctx.bot.wait_for("message", timeout=timeout, check=check)
                            start_date = self.parse_date(msg.content)
                            if not start_date:
                                await ctx.send("Couldn't parse date")
                                continue
                            e.start = start_date
                            ft = "Start date"
                    elif "url" in cnt or "link" in cnt:
                        await ctx.send("Enter new url for " + e.name)
                        msg = await ctx.bot.wait_for("message", timeout=timeout, check=check)
                        e.url = msg.content
                        ft = "Url"
                    else:
                        await ctx.send("Choose a feature from (name, start date, end date, url)")

                if ft:
                    with open("events.json", "w+") as f:
                        json.dump({
                            "events": [event.to_json() for event in ctx.bot.events],
                            "sales": [sale.to_json() for sale in ctx.bot.sales]
                        }, f, indent=4)
                    await ctx.send(ft + " for the event has been changed")
        except asyncio.TimeoutError:
            await ctx.send("Stopped editng event (timed out)")

    @commands.command(aliases=["deleteevent"])
    async def delevent(self, ctx):
        perm = ctx.author.guild_permissions
        owner = ctx.bot.owner
        if not perm.manage_messages and not owner:
            return
        check = lambda m: m.author == ctx.author
        timeout = 15
        deleted = False
        if len(ctx.message.content.split()) > 1:
            name = " ".join(ctx.message.content.lower().split()[1:])
        else:
            name = None
        try:
            while not deleted:
                if not name:
                    await ctx.send("Enter the name of event to delete:")
                    msg = await ctx.bot.wait_for("message", check=check, timeout=timeout)
                    name = msg.content.lower()
                for i in range(len(ctx.bot.events)):
                    if ctx.bot.events[i].name.lower() == name:
                        del ctx.bot.events[i]
                        deleted = True
                        break
                else:
                    for i in range(len(ctx.bot.sales)):
                        if ctx.bot.sales[i].name.lower() == name:
                            del ctx.bot.sales[i]
                            deleted = True
                            break
                    else:
                        await ctx.send("Couldn't find event with that name")
                        name = None
        except asyncio.TimeoutError:
            await ctx.send("Stopped deleting event (timed out)")

        if deleted:
            with open("events.json", "w+") as f:
                json.dump({
                    "events": [event.to_json() for event in ctx.bot.events],
                    "sales": [sale.to_json() for sale in ctx.bot.sales]
                }, f, indent=4)
            await ctx.send(name.capitalize() + " has been deleted")


