from discord.ext import commands


class MiscCog(commands.Cog):

    def __init__(self):
        pass

    @commands.command()
    async def clean(self, ctx):
        msg = ctx.message.clean_content
        msg = msg.replace("<", "< ")
        await ctx.send(msg)