import asyncio
import discord
import json
import time
import aiohttp
import math
import io
import re
import sys
import os
import datetime
import logging
from discord.ext import commands

from event import Event, EventsCog
from moderation import ModerationCog
from private import PrivateCog
from misc import MiscCog
import log
from news import NewsCog

logging.basicConfig(level=logging.WARNING)

dev = "--dev" in sys.argv or "-d" in sys.argv
if "-b" in sys.argv:
    backlog = int(sys.argv[sys.argv.index("-b") + 1])
elif "--backlog" in sys.argv:
    backlog = int(sys.argv[sys.argv.index("--backlog") + 1])
else:
    backlog = 1

bot_name = "Dev bot" if dev else "Vindictus Bot"
print("Starting " + bot_name)
if backlog > 1:
    print("Parsing a backlog of " + str(backlog) + " pages.")

token_file = "token_dev.txt" if dev else "token.txt"
config_file = "dev.config" if dev else "bot.config"

with open(token_file) as f:
    token = f.read()

if not "messages.json" in os.listdir():
    sent_messages = []
    with open("messages.json", "w+") as f:
        json.dump({"messages": sent_messages}, f, indent=4)
else:
    with open("messages.json") as f:
        sent_messages = json.load(f)["messages"]

with open(config_file) as f:
    configs = json.load(f)

if not "notifications.json" in os.listdir():
    notifications = []
    with open("notifications.json", "w+") as f:
        json.dump(notifications, f, indent=4)
else:
    with open("notifications.json") as f:
        notifications = json.load(f)

log_file = "log.log"

try:
    open(log_file).close()
except FileNotFoundError:
    open(log_file, "w+").close()

with open("events.json") as events_json:
    events_sales = json.load(events_json)
    events = [Event(jjson=x) for x in events_sales["events"]]
    sales = [Event(jjson=x) for x in events_sales["sales"]]


wolfram_appid = "7W664G-6TT5XQA4XX"
wolfram_url = "http://api.wolframalpha.com/v1/result"

bot = commands.Bot(command_prefix="!")
bot.events = events
bot.sales = sales
bot.configs = configs
bot.sent_messages = sent_messages


@bot.listen()
async def on_ready():
    bot.post_channels = []

    guilds_in_configs = configs["guilds"].keys()
    for guild in bot.guilds:
        if not str(guild.id) in guilds_in_configs:
            configs["guilds"][str(guild.id)] = configs["base"].copy()
        post_channel = configs["guilds"][str(guild.id)]["news_channel"]

        if post_channel:
            post_channel = bot.get_channel(int(post_channel))
        else:
            secondary_channel = None
            for channel in guild.channels:
                if channel.name == "news":
                    post_channel = channel
                    break
                elif channel.name == "general":
                    secondary_channel = channel
            if not post_channel and secondary_channel:
                post_channel = secondary_channel
            if post_channel:
                configs["guilds"][str(guild.id)]["news_channel"] = post_channel.id
        
        if post_channel:
            bot.post_channels.append(post_channel)
            print("Posting to: " + post_channel.name + " in " + guild.name)

    appinfo = await bot.application_info()
    bot.owner = appinfo.owner
    log.printlog(str(datetime.datetime.now()) + ": Ready")

bot.add_cog(EventsCog())
bot.add_cog(ModerationCog())
bot.add_cog(NewsCog(bot, backlog))
bot.add_cog(MiscCog())
bot.add_cog(PrivateCog())
bot.run(token)

with open(config_file, "w") as f:
    json.dump(bot.configs, f, indent=4)
