import discord
from discord.ext import commands

class ModerationCog(commands.Cog):

    @commands.command()
    async def game(self, ctx):
        if ctx.author != ctx.bot.owner:
            return
        await ctx.bot.change_presence(
            activity=discord.Game(name = " ".join(ctx.message.content.split()[1:]))
        )
        await ctx.message.add_reaction("👍")

    @commands.command()
    async def delmsg(self, ctx):
        if ctx.author != ctx.bot.owner:
            return
        split = ctx.message.content.split()
        msgid = int(split[-1])
        msg = None
        if len(split) == 2:
            msg = discord.utils.find(lambda m: m.id == msgid, ctx.bot.cached_messages)
            if not msg:
                await ctx.send("No such message found, try !delmsg [ch id] [msg id]")
                return
        elif len(split) == 3:
            chid = int(split[-2])
            ch = ctx.bot.get_channel(chid)
            if not ch:
                await ctx.send("Couldn't find a channel with this id.")
                return
            else:
                msg = await ch.fetch_message(msgid)
                if not msg:
                    await ctx.send("Couldn't find a message with that id in that channel")
                    return
        else:
            await ctx.send("Incorrect parameter count")
        
        if msg:
            try:
                await msg.delete()
                await ctx.message.add_reaction("👍")
            except:
                await ctx.message.add_reaction("👎")

    
    @commands.command()
    async def channel(self, ctx):
        perm = ctx.author.guild_permissions
        owner = ctx.author == ctx.bot.owner
        if not perm.manage_channels and not owner:
            return
        ments = ctx.message.channel_mentions
        if len(ments) != 1:
            await ctx.send("No channel mention or too many mentions")
            return
        ch = ctx.message.channel_mentions[0]
        perms = ch.permissions_for(ctx.me)
        if not perms.send_messages:
            await ctx.send("I don't have the permission to send messages in that channel, news channel not changed")
            return
        to_del = None
        for c in range(len(ctx.bot.post_channels)):
            if ctx.bot.post_channels[c].guild == ctx.guild:
                to_del = c
                break
        del ctx.bot.post_channels[c]
        ctx.bot.post_channels.append(ch)
        ctx.bot.configs["guilds"][str(ctx.guild.id)]["news_channel"] = ch.id
        await ctx.send("Posting news to " + ch.mention)
        
                
        